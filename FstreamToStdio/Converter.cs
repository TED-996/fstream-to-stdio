﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace FstreamToStdio {
	public class Converter {
		public string ErrorMessage { get; private set; }

		public Converter() {
			ErrorMessage = "Error encountered. Either the C++ file is badly formed, you are using #define, macros or " +
				"comments in the file input/output instructions, or there is a bug with this program.";
		}

		public string ConvertFile(string filename) {
			try {
				return ConvertString(File.ReadAllText(filename));
			}
			catch {
				return null;
			}
		}

		string ConvertString(string text) {
			int tokenIndex = text.IndexOf("ifstream", StringComparison.InvariantCulture);
			List<string> streamNames = new List<string>(); //the object name, such as "fin"
			List<StreamType> streamTypes = new List<StreamType>(); //the type of stream, such as StreamType.Input
			text = ReplaceHeader(text);
			while (tokenIndex != -1) {
				if (InComment(text, tokenIndex)) {
					tokenIndex = text.IndexOf("ifstream", tokenIndex + 1, StringComparison.InvariantCulture);
					continue;
				}
				text = ProcessIfstream(text, tokenIndex, streamNames);
				streamTypes.Add(StreamType.Input);
				tokenIndex = text.IndexOf("ifstream", StringComparison.InvariantCulture);
			}

			tokenIndex = text.IndexOf("ofstream", StringComparison.InvariantCulture);
			while (tokenIndex != -1) {
				if (InComment(text, tokenIndex)) {
					tokenIndex = text.IndexOf("ofstream", tokenIndex + 1, StringComparison.InvariantCulture);
					continue;
				}
				text = ProcessOfstream(text, tokenIndex, streamNames);
				streamTypes.Add(StreamType.Output);
				tokenIndex = text.IndexOf("ofstream", StringComparison.InvariantCulture);
			}
			for (int i = 0; i < streamNames.Count; i++) {
				text = ProcessObjectCalls(text, streamNames[i], streamTypes[i]);
			}

			return text;
		}

		static string ProcessObjectCalls(string text, string objectName, StreamType objectType) {
			int tokenStart = text.IndexOf(objectName, StringComparison.InvariantCulture);
			while (tokenStart != -1) {
				string line;
				if (tokenStart == -1) {
					break;
				}
				if (InComment(text, tokenStart)) {
					tokenStart = text.IndexOf(objectName, tokenStart + 1, StringComparison.InvariantCulture);
					continue;
				}
				int index = objectName.Length;

				line = text.Substring(tokenStart, text.IndexOf(';', tokenStart) - tokenStart);
				line = RemoveSpaces(line);

				if (index == line.Length) {
					tokenStart = text.IndexOf(objectName, tokenStart + 1, StringComparison.InvariantCulture);
					continue;
				}
				if (line[index] == '.') {
					index++;

					if (line.IndexOf("open", StringComparison.InvariantCulture) == index) {
						line = ProcessFileOpening(line, objectType);
					}
					else if (line.IndexOf("close", StringComparison.InvariantCulture) == index) {
						line = ProcessFileClosing(line);
					}
				}
				else if (line[index] == '>' && objectType == StreamType.Input) {
					line = ProcessStreamInput(line, text, tokenStart);
				}
				else if (line[index] == '<' && objectType == StreamType.Output) {
					line = ProcessStreamOutput(line, text, tokenStart);
				}
				else {
					tokenStart = text.IndexOf(objectName, tokenStart + 1, StringComparison.InvariantCulture);
					continue;
				}


				//Restore the line
				text = text.Substring(0, tokenStart) + line + text.Substring(text.IndexOf(';', tokenStart) + 1);
			}



			return text;
		}

		static string ReplaceHeader(string text) {
			int headerIndex = text.IndexOf("<fstream>", StringComparison.InvariantCulture);
			return text.Substring(0, headerIndex) + "<stdio.h>" + text.Substring(headerIndex + 9);

		}

		static string ProcessIfstream(string text, int tokenAppearance, List<string> streamNames) {
			int lineEnd = text.IndexOf(';', tokenAppearance);
			string line = text.Substring(tokenAppearance, lineEnd - tokenAppearance).TrimEnd();
			StringBuilder result = new StringBuilder();
			int nameStart;
			string name;
			if (line.Contains("(")) {
				//Must split into declaration and opening.
				result.Append("FILE* ");
				nameStart = line.IndexOf(' ') + 1;
				name = line.Substring(nameStart, line.IndexOf('(') - nameStart).Trim();
				result.Append(name);
				result.Append(" = fopen(");
				streamNames.Add(name);
				int filenameStart = line.IndexOf('(') + 1;
				string filename = line.Substring(filenameStart, line.IndexOf(')') - filenameStart).Trim();
				result.Append(filename + ", \"r\");");
			}
			else {
				result.Append("FILE* ");
				nameStart = line.IndexOf(' ') + 1;
				name = line.Substring(nameStart).Trim();
				streamNames.Add(name);
				result.Append(name);
				result.Append(";");
			}


			return text.Substring(0, tokenAppearance) + result + text.Substring(lineEnd + 1);
		}

		static string ProcessOfstream(string text, int tokenAppearance, List<string> streamNames) {
			int lineEnd = text.IndexOf(';', tokenAppearance);
			string line = text.Substring(tokenAppearance, lineEnd - tokenAppearance).TrimEnd();
			StringBuilder result = new StringBuilder();
			int nameStart;
			string name;
			if (line.Contains("(")) {
				//Must split into declaration and opening.
				result.Append("FILE* ");
				nameStart = line.IndexOf(' ') + 1;
				name = line.Substring(nameStart, line.IndexOf('(') - nameStart).Trim();
				result.Append(name);
				result.Append(" = fopen(");
				streamNames.Add(name);
				int filenameStart = line.IndexOf('(') + 1;
				string filename = line.Substring(filenameStart, line.IndexOf(')') - filenameStart).Trim();
				result.Append(filename + ", \"w\");");
			}
			else {
				result.Append("FILE* ");
				nameStart = line.IndexOf(' ') + 1;
				name = line.Substring(nameStart).Trim();
				streamNames.Add(name);
				result.Append(name);
				result.Append(";");
			}


			return text.Substring(0, tokenAppearance) + result + text.Substring(lineEnd + 1);
		}


		static string ProcessFileOpening(string line, StreamType objectType) {
			int period = line.IndexOf('.');
			return line.Substring(0, period) + " = fopen(" + line.Substring(period + 6, line.Length - period - 7) +
			                 ", \"" + (objectType == StreamType.Input ? "r" : "w") + "\");";
		}

		static string ProcessFileClosing(string line) {
			int period = line.IndexOf('.');
			//return line.Substring(0, period) + " = fclose(" + line.Substring(period + 6, line.Length - period - 8)
			// + ");";
			return "fclose(" + line.Substring(0, period) + ");";
		}

		static string ProcessStreamInput(string line, string file, int textIndex) {
			int index = line.IndexOf('>');
			string newLine = "fscanf(" + line.Substring(0, index) + ", ";
			StringBuilder format = new StringBuilder();
			List<String> identifiers = GetIdentifiers(line.Substring(index, line.Length - index));
			foreach (string identifier in identifiers) {
				format.Append(GetFormatToken(identifier, file, textIndex));
			}
			newLine += "\"" + format + "\"";
			StringBuilder arguments = new StringBuilder();
			foreach (string identifier in identifiers) {
				arguments.Append(", &" + identifier);
			}
			newLine += arguments.ToString() + ");";

			return newLine;
		}

		static string ProcessStreamOutput(string line, string file, int textIndex) {
			int index = line.IndexOf('<');
			string newLine = "fprintf(" + line.Substring(0, index) + ", ";
			StringBuilder format = new StringBuilder();
			List<String> identifiers = GetIdentifiers(line.Substring(index, line.Length - index));
			foreach (string identifier in identifiers) {
				format.Append(GetFormatToken(identifier, file, textIndex));
			}
			newLine += "\"" + format + "\"";
			StringBuilder arguments = new StringBuilder();
			foreach (string identifier in identifiers) {
				if (IsIdentifier(identifier)) {
					arguments.Append(", " + identifier);
				}
			}
			newLine += arguments + ");";

			return newLine;
		}

		static List<string> GetIdentifiers(string line) {
			int index = 0;
			List<string> result = new List<string>();
			while (true) {
				if (index == line.Length) {
					break;
				}
				if (!(line[index] == '<' && line[index + 1] == '<') && !(line[index] == '>' && line[index + 1] == '>')) {
					throw new ApplicationException("Bad stream format.");
				}
				index += 2;
				int nextIndex = line.IndexOfAny(new[] {'>', '<'}, index);
				if (nextIndex == -1) {
					result.Add(line.Substring(index));
					break;
				}
				result.Add(line.Substring(index, nextIndex - index));
				index = nextIndex;
				if (nextIndex == line.Length + 1) {
					break;
				}
			}
			return result;
		}

		static string GetFormatToken(string identifier, string file, int index) {
			if (identifier[0] == '\'' && identifier[identifier.Length - 1] == '\'') {
				return identifier.Substring(1, identifier.Length - 2);
			}
			if (identifier[0] == '\"' && identifier[identifier.Length - 1] == '\"') {
				return identifier.Substring(1, identifier.Length - 2);
			}
			if (char.IsDigit(identifier[0])) {
				return identifier;
			}
			if (identifier == "true" || identifier == "false") {
				return identifier;
			}
			//Find the declaration;
			string declaration = GetDeclaration(identifier, file, index);
			if (declaration == "int" || declaration == "bool") {
				return "%d";
			}
			if (declaration == "short") {
				return "%hd";
			}
			if (declaration == "char") {
				return "%c";
			}
			if (declaration == "float" || declaration == "double") {
				return "%f";
			}
			if (declaration == "char*") {
				return "%s";
			}
			if (declaration == "long long") {
				return "%lld";
			}



			return "%d";
		}

		static bool IsIdentifier(string identifier) {
			if (identifier[0] == '\'' && identifier[identifier.Length - 1] == '\'') {
				return false;
			}
			if (identifier[0] == '\"' && identifier[identifier.Length - 1] == '\"') {
				return false;
			}
			if (char.IsDigit(identifier[0])) {
				return false;
			}
			if (identifier == "true" || identifier == "false") {
				return false;
			}
			return true;
		}

		static string GetDeclaration(string identifier, string file, int index) {
			int depth = 0;
			int minDepth = 0;
			BitArray isInScopeArray = new BitArray(index + 1);
			for (int i = index; i >= 0; i--) {
				if (file[index] == '{') {
					depth--;
					if (depth < minDepth) {
						minDepth = depth;
					}
				}
				if (file[index] == '}') {
					depth++;
				}
				isInScopeArray[i] = depth == minDepth;
			}
			string[] dataTypes = new[] {"long long", "short", "int", "bool", "char", "float", "double"};
			foreach (string type in dataTypes) {
				int tokenIndex = file.IndexOf(type, StringComparison.InvariantCulture);
				while (tokenIndex != -1 && tokenIndex <= index) {
					if (!isInScopeArray[tokenIndex]) {
						tokenIndex = file.IndexOf(type, tokenIndex + 1, StringComparison.InvariantCulture);
						continue;
					}
					if (!"\n \t;{}(".Contains(file[tokenIndex - 1].ToString(CultureInfo.InvariantCulture))) {
						tokenIndex = file.IndexOf(type, tokenIndex + 1, StringComparison.InvariantCulture);
						continue;
					}
					if (!"\n \t".Contains(file[tokenIndex + type.Length].ToString(CultureInfo.InvariantCulture))) {
						tokenIndex = file.IndexOf(type, tokenIndex + 1, StringComparison.InvariantCulture);
						continue;
					}
					string line = file.Substring(tokenIndex + type.Length,
						file.IndexOf(';', tokenIndex) - tokenIndex - type.Length);

					int identifierIndex = GetIdentifierIndex(line, identifier);
					if (identifierIndex != -1) {
						if (IsPointer(identifierIndex, line)) {
							return type + '*';
						}
						return type;
					}


					tokenIndex = file.IndexOf(type, tokenIndex + 1, StringComparison.InvariantCulture);
				}
			}
			return "int";
		}

		static int GetIdentifierIndex(string line, string identifier) {
			line = line.Trim();
			if (line.StartsWith("int")) {
				line = line.Substring(3).Trim();
			}
			line = "," + RemoveWhiteSpace(line) + ",";
			if (IsFunctionHeader(line)) {
				return -1;
			}
			int result = line.IndexOf(',' + identifier + ',', StringComparison.InvariantCulture);
			if (result != -1) {
				return result;
			}
			result = line.IndexOf('*' + identifier + ',', StringComparison.InvariantCulture);
			if (result != -1) {
				return result;
			}
			result = line.IndexOf(',' + identifier + '[', StringComparison.InvariantCulture);
			if (result != -1) {
				return result;
			}
			result = line.IndexOf('*' + identifier + '[', StringComparison.InvariantCulture);
			if (result != -1) {
				return result;
			}
			return -1;
		}

		static bool IsFunctionHeader(string line) {
			int index = 1;
			while (char.IsLetterOrDigit(line[index]) || char.IsWhiteSpace(line[index])) {
				index++;
			}
			if (line[index] == '(') {
				return true;
			}
			return false;
		}

		static bool IsPointer(int identifierIndex, string line) {
			line = line.Trim();
			if (identifierIndex != 0 && line[identifierIndex - 1] == '*') {
				return true;
			}
			while (identifierIndex < line.Length && char.IsLetterOrDigit(line[identifierIndex])) {
				identifierIndex++;
			}
			if (identifierIndex == line.Length || line[identifierIndex] != '[') {
				return false;
			}
			return true;
		}

		static string RemoveWhiteSpace(string line) {
			StringBuilder builder = new StringBuilder();
			foreach (char ch in line.Where(ch => !char.IsWhiteSpace(ch))) {
				builder.Append(ch);
			}
			return builder.ToString();
		}

		/*static bool IsGlobal(string text, int index) {
			int depth = 0;
			for (int i = 0; i < index; i++) {
				if (text[i] == '{') {
					depth++;
				}
				else if (text[i] == '}') {
					depth--;
				}
			}
			return depth == 0;
		}*/

		static bool InComment(string text, int index) {
			bool inLineComment = false;
			bool inBlockComment = false;
			int i;
			int maxIndex = Math.Min(index, text.Length - 1);
			for (i = 0; i < maxIndex; i++) {
				if (text[i] == '/' && text[i + 1] == '/') {
					inLineComment = true;
				}
				if (text[i] == '\n') {
					inLineComment = false;
				}
				if (text[i] == '/' && text[i + 1] == '*') {
					inBlockComment = true;
					i++; //prevent the /*/ case on the next step.
				}
				if (text[i] == '*' && text[i + 1] == '/') {
					inBlockComment = false;
				}
			}

			return inLineComment || inBlockComment;
		}

		static string RemoveSpaces(string str) {
			StringBuilder builder = new StringBuilder();
			bool inQuote = false;
			QuoteType quoteType = QuoteType.None;
			for (int i = 0; i < str.Length; i++) {
				char c = str[i];
				if (c == '\"') {
					if (!inQuote) {
						inQuote = true;
						quoteType = QuoteType.DoubleQuotes;
					}
					else if (quoteType == QuoteType.DoubleQuotes && (i == 0 || str[i - 1] != '\\')) { //not escaped
						inQuote = false;
						quoteType = QuoteType.None;
					}
				}
				if (c == '\'') {
					if (!inQuote) {
						inQuote = true;
						quoteType = QuoteType.SingleQuotes;
					}
					else if (quoteType == QuoteType.SingleQuotes && (i == 0 || str[i - 1] != '\\')) { //not escaped
						inQuote = false;
						quoteType = QuoteType.None;
					}
				}
				if (!char.IsWhiteSpace(c) || inQuote) {
					builder.Append(c);
				}
			}
			return builder.ToString();
		}
	}

	enum StreamType {
		Input, Output
	}

	enum QuoteType {
		DoubleQuotes, SingleQuotes, None
	}
}