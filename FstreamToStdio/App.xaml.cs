﻿using System;
using System.Windows;

namespace FstreamToStdio {
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App :Application {
	}

	public class StartUp :Application {
		[System.STAThreadAttribute()]
		[System.Diagnostics.DebuggerNonUserCodeAttribute()]
		public static void Main() {
			StartUp app = new StartUp();
			app.InitializeComponent();
			app.Run();
		}

		public void InitializeComponent() {
			this.StartupUri = new Uri("MainWindow.xaml", System.UriKind.Relative);
		}
	}
}
