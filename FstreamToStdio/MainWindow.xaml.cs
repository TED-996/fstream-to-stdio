﻿using System.IO;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Win32;

namespace FstreamToStdio {
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow {
		public MainWindow() {
			InitializeComponent();
		}

		readonly string currentDirectory = Directory.GetCurrentDirectory();
		readonly Converter converter = new Converter();

		void BrowseInputFile(object sender, RoutedEventArgs routedEventArgs) {
			OpenFileDialog dialog = new OpenFileDialog {
				DefaultExt = ".cpp",
				Filter = "C++ files (*.cpp)|*.cpp|All files|*",
				Multiselect = false,
				Title = "Select input file...",
				InitialDirectory = currentDirectory
			};

			bool? result = dialog.ShowDialog(this);
			if (result.Value) {
				InputFilename.Text = dialog.FileName;
			}
			InputFilenameChanged(null, null);
		}

		private void BrowseOutputClick(object sender, RoutedEventArgs e) {
			SaveFileDialog dialog = new SaveFileDialog {
				DefaultExt = ".cpp;*.c",
				Filter = "C/C++ files (*.cpp, *.c)|*.cpp;*.c|All files|*",
				Title = "Select output file...",
				InitialDirectory = GetOutputDir()
			};

			bool? result = dialog.ShowDialog(this);
			if (result.Value) {
				OutputFilename.Text = dialog.FileName;
			}
		}

		string GetOutputDir() {
			if (string.IsNullOrEmpty(InputFilename.Text)) {
				return currentDirectory;
			}
			string directory = Path.GetDirectoryName(InputFilename.Text);
			if (string.IsNullOrEmpty(directory)) {
				return currentDirectory;
			}
			return Directory.Exists(directory) ? Path.GetDirectoryName(InputFilename.Text) : currentDirectory;
		}

		private void EnableCustomOutput(object sender, RoutedEventArgs e) {
			OutputFilename.IsEnabled = true;
			OutputBrowseButton.IsEnabled = true;
			SaveSeparately.IsEnabled = false;
		}

		private void DisableCustomOutput(object sender, RoutedEventArgs e) {
			OutputFilename.IsEnabled = false;
			OutputBrowseButton.IsEnabled = false;
			SaveSeparately.IsEnabled = true;
			OutputFilename.Text = GetDefaultOutput();
		}

		string GetDefaultOutput() {
			if (ChangeOutput.IsChecked.GetValueOrDefault()) {
				return OutputFilename.Text;
			}
			if (!SaveSeparately.IsChecked.GetValueOrDefault()) {
				return InputFilename.Text;
			}
			int extensionStart = InputFilename.Text.LastIndexOf('.');
			if (extensionStart == -1) {
				OutputFilename.Text = InputFilename.Text;
			}
			return InputFilename.Text.Substring(0, extensionStart) + "_stdio" + 
				InputFilename.Text.Substring(extensionStart);
		}

		private void ConvertClick(object sender, RoutedEventArgs e) {
			bool result = Convert();
			MainTabControl.SelectedIndex = 1;
			if (!result) {
				ResultText.Text = converter.ErrorMessage;
			}
			else {
				ResultText.Text = "Success! You can convert another file or close the program.\nDon't forget to check the"
					+ "variable types (%d, %c, %f, etc) to be correct because intepreting this is extremely difficult. "
					+ "The program tries to intepret them, but does not always get them right.";
			}

		}

		private bool Convert() {
			string result = converter.ConvertFile(InputFilename.Text);
			if (string.IsNullOrEmpty(result)) {
				return false;
			}
			try {
				if (BackupCheckbox.IsChecked.GetValueOrDefault()) {
					if (!SaveBackup()) {
						return false;
					}
				}
				string dir = Path.GetDirectoryName(OutputFilename.Text);
				if (string.IsNullOrEmpty(dir)) {
					return false;
				}
				if (!Directory.Exists(dir)) {
					Directory.CreateDirectory(dir);
				}
				File.WriteAllText(OutputFilename.Text, result);
				return true;
			}
			catch {
				return false;
			}
		}

		private void InputFilenameChanged(object sender, TextChangedEventArgs e) {
			try {
				ConvertButton.IsEnabled = File.Exists(InputFilename.Text);
			}
			catch {
				ConvertButton.IsEnabled = false;
			}
			if (ChangeOutput.IsChecked.HasValue && ChangeOutput.IsChecked.Value) {
				return;
			}
			OutputFilename.Text = GetDefaultOutput();
		}

		private bool SaveBackup() {
			try {
				int extensionStart = InputFilename.Text.LastIndexOf('.');
				string backupFilename = InputFilename.Text.Substring(0, extensionStart) + "_fstream" + 
					InputFilename.Text.Substring(extensionStart);
				File.Copy(InputFilename.Text, backupFilename, true);

				return true;
			}
			catch {
				return false;
			}
		}

		void SaveSeparatelyChecked(object sender, RoutedEventArgs e) {
			OutputFilename.Text = GetDefaultOutput();
		}

		void SaveSeparatelyUnchecked(object sender, RoutedEventArgs e) {
			OutputFilename.Text = GetDefaultOutput();
		}
	}
}
